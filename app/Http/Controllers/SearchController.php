<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SearchRequest;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Client\Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class SearchController extends Controller
{
    public function index(){
        return view('search');
    }

    public function search(Request $req){

        if($req->input("prev") !== null && $req->input("page") == true){
            $inicioPage  = ($req->input("page") !== null && $req->input("page") > 0)? $req->input("page") - 1 : 1 ;
        }else{
            $inicioPage  = ($req->input("page") !== null)? $req->input("page") + 1 : 2 ;
        }
        
        $search = ($req->input("language") !== null)? $req->input("language") : $req->input("search")  ;
       
         
        // Inicia
       $curl = curl_init();
       $url = "https://api.github.com/search/repositories?q=language:".$search."&sort=stars&page=".$inicioPage;
        // Configura
    
        curl_setopt($curl,  CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERAGENT,'Request');

       $response = curl_exec($curl);

    
        curl_close($curl);

      
       $repositorio = json_decode($response, true);
       
       $repositorios = $repositorio["items"];    
       $totalPage = ceil( $repositorio["total_count"] / 30);
     
   
       
       $pesquisa = $search;
        //  dd($repositorio["items"]);
  
       return view('search', compact('repositorios', 'pesquisa', 'inicioPage', 'totalPage'));
   
      }
    public function searchPage($page = 1) {
       
    }
}
