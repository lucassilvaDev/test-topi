<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Test TOPi</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
         <!-- Compiled and minified CSS -->
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
            
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    </head>
    <body style="background: #26a69a;height: 100%;">
        <div class="container z-depth-5" style="background: #ffffff; padding:25px; height: 100%;">

            <div class="row">
                <h2>Lista de Repositorios: {{ isset($pesquisa)?$pesquisa : "" }}</h2>
        
                <form name="formCad" class="col s12" id="formCad" method="post" action="{{url('search')}}">
                    @csrf
                    <div class="input-field col s6">
                       
                        <input type="text" id="first_name" value="{{ isset($pesquisa)?  $pesquisa : '' }} {{old('language')}}" name="language" id="">
                        <label for="first_name">Linguagem</label>
                      </div>
                      <div class="input-field col s3">
                    <button type="submit" class="btn btn-success col s12">Pesquisar</button>
                      </div>
                      <div class="input-field col s3">
                        <a href="{{url('/')}}" class="btn btn-success col s12">Limpar</a>
                    </div>
                </form>
               
            </div>
            

            <div class="content">
            <div>
           
                <table border="1">
                    <tr>
                        <th>#</th>
                        <th>Nome do repositório</th>
                        <th>Número de estrelas</th>
                        <th>Número de garfos</th>
                    </tr>
                    @if(isset($repositorios))
                    @foreach ( $repositorios as $repositorio )
                    <tr>
                        <td><img src="{{ $repositorio["owner"]["avatar_url"] }}" alt="" class="circle responsive-img" style="width: 50px;"> </td>
                        <td> {{ $repositorio["name"] }}</td>
                        <td> {{ $repositorio["stargazers_count"] }}<i class="material-icons tiny">star</i></td>
                        <td> {{ $repositorio["forks"] }}</td>
                    </tr>
                    @endforeach
                    @else
                    
                    @endif
                </table>
                
                @if(isset($inicioPage))
                <div class="row">
                    <div class=" col s4">  
                <form name="formCad" id="formCad" method="post" action="{{url('search')}}">
                    @csrf
                    <input type="hidden" value="{{$inicioPage}}" name="page">
                    <input type="hidden" value="{{$pesquisa}}" name="search">
                    <input type="hidden" value="true" name="prev">
                    <button type="submit" class="btn btn-success col s12"> Anterior</button>
                </form>
                    </div>
                    <div class=" col s4 center">  
            <p>Página {{$inicioPage}} de {{$totalPage}}</p>
        </div>
        <div class=" col s4"> 
                <form name="formCad" id="formCad" method="post" action="{{url('search')}}">
                    @csrf
                    <input type="hidden" value="{{$inicioPage}}" name="page">
                    <input type="hidden" value="{{$pesquisa}}" name="search">
                    <button type="submit" class="btn btn-success col s12"> Proximo</button>
                </form>
            </div>
            </div>
                @endif
            </div>

            
            </div>
        </div>
    </body>
 ''
</html>
